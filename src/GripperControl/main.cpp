#include <string.h>
#include <stdio.h>

#include <unistd.h>     //Used for UART
#include <fcntl.h>      //Used for UART
#include <termios.h>    //Used for UART

//#include "UARTInterface.h" //UART Interface
#include "MotorController.h" //Motor Controller
#include "Utilities.hpp"

int setupUART();
void setBrake();
void releaseBrake();

//UART Handler
int uart0_filestream;

//Setup the UART on the Raspberry Pi
int setupUART() {
    //At bootup, pins 8 and 10 are already set to UART0_TXD, UART0_RXD (ie the alt0 function) respectively
    uart0_filestream = -1;
    struct termios options;

    //Open the UART Serial Interface
    uart0_filestream = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY); //Open in non blocking read/write mode

    if(uart0_filestream == -1) {
        printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
        return 0;
    } else {
        //Attach TERMIOS to UART, with settings
        tcgetattr(uart0_filestream, &options);
        options.c_cflag = B2400 | CS8 | CLOCAL | CREAD; //2400 Baud Rate, 8-Bit, No Parity, One Stop Bit, Read Enabled
        options.c_iflag = IGNPAR; //Ignore Parity Checks
        options.c_oflag = 0;
        options.c_lflag = 0;
        tcflush(uart0_filestream, TCIFLUSH); //Flush any non-read/written data
        tcsetattr(uart0_filestream, TCSANOW, &options); //Write modified attributes back
        return 1;
    }
}

int main(int argc, char* argv[]) {
    MotorController gripperMotor;

    if(setupUART()) {
        unsigned char tx_buffer[20];
        unsigned char *p_tx_buffer;
        int count = 0;

        unsigned char rx_buffer[256];
        int rx_length = 0;

        int i = 0;

        delay(500);

        //----- TX BYTES -----
        p_tx_buffer = &tx_buffer[0];
        *p_tx_buffer++ = 0x55; //CMD Sync
        *p_tx_buffer++ = 0x83; //CMD SPEED
        *p_tx_buffer++ = 0x7F; //SPEED - 50%

        if (uart0_filestream != -1)
        {
            count = write(uart0_filestream, &tx_buffer[0], (p_tx_buffer - &tx_buffer[0]));      //Filestream, bytes to write, number of bytes to write
            if (count < 0)
            {
                printf("UART TX error\n");
            }
        }

        delay(50);

        //----- CHECK FOR ANY RX BYTES -----
        if (uart0_filestream != -1)
        {
            // Read up to 255 characters from the port if they are there
            rx_length = read(uart0_filestream, (void*)rx_buffer, 255);      //Filestream, buffer to store in, number of bytes to read (max)
            if (rx_length < 0)
            {
                printf("ERROR\n");
                //An error occured (will occur if there are no bytes)
            }
            else if (rx_length == 0)
            {
                printf("LENGTH\n");
            }
            else
            {
                //Bytes received
                rx_buffer[rx_length] = '\0';
                printf("%i bytes read : ", rx_length, rx_buffer);
                for(i = 0; i < rx_length; i++) {
                    printf("%s ", byte_to_binary(rx_buffer[i]));
                }
                printf("\n");
            }
        }

        delay(1000);

        //----- TX BYTES -----
        p_tx_buffer = &tx_buffer[0];
        *p_tx_buffer++ = 0x55; //CMD Sync
        *p_tx_buffer++ = 0x80; //CMD STOP

        if (uart0_filestream != -1)
        {
            count = write(uart0_filestream, &tx_buffer[0], (p_tx_buffer - &tx_buffer[0]));      //Filestream, bytes to write, number of bytes to write
            if (count < 0)
            {
                printf("UART TX error\n");
            }
        }

        delay(50);

        //----- CHECK FOR ANY RX BYTES -----
        if (uart0_filestream != -1)
        {
            // Read up to 255 characters from the port if they are there
            rx_length = read(uart0_filestream, (void*)rx_buffer, 255);      //Filestream, buffer to store in, number of bytes to read (max)
            if (rx_length < 0)
            {
                printf("ERROR\n");
                //An error occured (will occur if there are no bytes)
            }
            else if (rx_length == 0)
            {
                printf("LENGTH\n");
            }
            else
            {
                //Bytes received
                rx_buffer[rx_length] = '\0';
                printf("%i bytes read : ", rx_length, rx_buffer);
                for(i = 0; i < rx_length; i++) {
                    printf("%s ", byte_to_binary(rx_buffer[i]));
                }
                printf("\n");
            }
        }

        setBrake();

    } else {
        printf("OHSNAP\n");
    }

    close(uart0_filestream);

    return 0;
}
