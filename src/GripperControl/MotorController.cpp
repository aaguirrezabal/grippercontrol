/* 
 * File:   MotorController.cpp
 * Author: andoni
 * 
 * Created on August 26, 2014, 1:50 PM
 */

#include "MotorController.h"

#include <wiringPi.h>   //Used for GPIO

MotorController::MotorController() {
    wiringPiSetup();
    pinMode(4, OUTPUT);
    releaseBrake();
}

MotorController::~MotorController() {
    setBrake();
}

void MotorController::releaseBrake() {
    digitalWrite(4, HIGH);
}

void MotorController::setBrake() {
    // The Brake Line is inverted, so when you want to turn it on, 
    // you must set it to a logic 0
    digitalWrite(4, LOW);
}
