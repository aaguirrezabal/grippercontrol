#include "UARTInterface.h"

#include <stdio.h>

#include <fcntl.h>      //Used for UART
#include <termios.h>    //Used for UART
#include <unistd.h>     //Used for UART

// Constructor
UARTInterface::UARTInterface() {
    // Init the UART Filestream Variable
    uart_filestream = -1;
}

// Destructor
UARTInterface::~UARTInterface() {
    close(uart_filestream);
}

bool UARTInterface::initUART() {
    //Create a options variable;
    struct termios options;
    
    //Open the UART Serial Interface
    uart_filestream = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY); //Open in non blocking read/write mode

    if(uart_filestream == -1) {
        printf("Error - Unable to open UART.  Ensure it is not in use by"
               " another application\n");
        return false;
    } else {
        //Attach TERMIOS to UART, with settings
        tcgetattr(uart_filestream, &options);
        // 2400 Baud Rate, 8-Bit, No Parity, One Stop Bit, Read Enabled
        options.c_cflag = B2400 | CS8 | CLOCAL | CREAD; 
        options.c_iflag = IGNPAR; // Ignore Parity Checks
        options.c_oflag = 0;
        options.c_lflag = 0;
        tcflush(uart_filestream, TCIFLUSH); // Flush any non-read/written data
        // Write modified attributes back
        tcsetattr(uart_filestream, TCSANOW, &options); 
        return true;
    }
}

bool UARTInterface::receiveMessage() {
    
}

bool UARTInterface::sendMessage() {
    
}

