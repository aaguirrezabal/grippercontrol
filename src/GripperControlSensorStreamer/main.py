import spidev
import time

spi=spidev.SpiDev()
spi.open(0,0)

def readadc(adcnum):
	if adcnum>7 or adcnum<0:
		return -1
	r = spi.xfer2([1,(8 + adcnum<<4),0])
	adcout = (((r[1]&3)<<8) + r[2])
	return adcout

def main():
	while 1:
		print readadc(0)
		time.sleep(0.01)

if __name__=="__main__":
	main()
