/* 
 * File:   MotorController.h
 * Author: andoni
 *
 * Created on August 26, 2014, 1:50 PM
 */

#ifndef MOTORCONTROLLER_H
#define	MOTORCONTROLLER_H

class MotorController {
public:
    MotorController();
    ~MotorController();

public:
    void setBrake(void);
    void releaseBrake(void);
    
private:

};

#endif	/* MOTORCONTROLLER_H */

