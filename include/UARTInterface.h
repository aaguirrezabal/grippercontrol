#ifndef UARTINTERFACE_H
#define UARTINTERFACE_H
 

class UARTInterface {
public:
    UARTInterface(void);
    ~UARTInterface(void);

public:
    bool initUART(void);
    
    bool receiveMessage();
    bool sendMessage();
    
private:
    int uart_filestream;
};


#endif /* UARTINTERFACE_H */
