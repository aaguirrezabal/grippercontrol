//Forward Declarations
const char* byte_to_binary(int);

//Convert Decimal to Binary String
const char* byte_to_binary(int number) {
    static char b[9];
    b[0] = '\0';

    int x;
    for (x = 128; x > 0; x >>= 1) {
        strcat(b, ((number & x) == x) ? "1" : "0");
    }

    return b;
}
